# DO380 - Red Hat OpenShift Administration III: Scaling Kubernetes Deployments in the Enterprise 

## Plan, implement, and manage OpenShift clusters at scale

Red Hat OpenShift Administration III: Scaling Kubernetes Deployments in the Enterprise (DO380) expands upon the skills required to plan, implement, and manage OpenShift® clusters in the enterprise. You will learn how to support a growing number of stakeholders, applications, and users to achieve large-scale deployments.

## This course is based on Red Hat® OpenShift Container Platform 4.5.

# DO380-apps
- Last check on 20210805, DO380-apps doesn't exists on github.com/redhattraining
